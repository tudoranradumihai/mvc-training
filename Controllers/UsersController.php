<?php 

Class UsersController extends Controller {

	public function defaultAction(){
		self::listAction();
	}

	public function listAction(){
		$this->view->render(__METHOD__);
	}

}