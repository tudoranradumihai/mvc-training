<?php

Class Database {
	
	public $connection;

	public function __construct(){
		$configuration = require "Configuration/EnvironmentConfiguration.php";
		$this->connection = mysqli_connect(
			$configuration["DATABASE"]["HOSTNAME"],
			$configuration["DATABASE"]["USERNAME"],
			$configuration["DATABASE"]["PASSWORD"],
			$configuration["DATABASE"]["DATABASE"]
		);
		if (!$this->connection) {
		    echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
		    echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
		    echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
		    exit;
		}
	}

	public function executeQuery($query){
		return mysqli_query($this->connection, $query);
	}

	public function lastID(){
		return mysqli_insert_id($this->connection);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}

}