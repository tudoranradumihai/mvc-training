<?php

Class URLBuilder {

	public static function create($controller, $action = "default", $uid = NULL) {
		if (class_exists($controller."Controller")){
			$configuration = require "Configuration/EnvironmentConfiguration.php";
			$path = "";
			if (isset($configuration["GENERAL"]["ABSOLUTE_PATH"])){
				$path = $configuration["GENERAL"]["DOMAIN"];
			}
			if (isset($configuration["GENERAL"]["REWRITE_MODE"])){
				$path .= $controller."/".$action;
				if ($uid){
					$path .= "/".$uid;
				}
			} else {
				$path .= "index.php?C=".$controller."&A=".$action;
				if ($uid){
					$path .= "&UID=".$uid;
				}
			}
			return $path;
		} else {
			return NULL;
		}
	}

	public static function redirect($controller,$action = "default",$uid = NULL) {
		$path = self::create($controller, $action, $uid);
		echo "<meta http-equiv='refresh' content='0; url=".$path."' />";
	}

}