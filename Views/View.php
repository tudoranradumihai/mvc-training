<?php

Class View {

	protected $variables = array();

	public function set($name,$value){
		$protectedVariableNames = array("controller","action");
		if(!in_array($name,$protectedVariableNames)){
			$this->variables[$name] = $value;
		} else {
			throw new Exception('Protected variable name "'.$name.'" usage attempt.');
		}
	}

	public function render($method){
		$method = explode("::",$method);
		$controller = preg_replace("/Controller$/","",$method[0]);
		$action = preg_replace("/Action$/","",$method[1]);
		foreach ($this->variables as $key => $value){
			$$key = $value;
		}
		require "Views/$controller/$action.php";
	}

}