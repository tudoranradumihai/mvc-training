<?php

Class Users {

	public $id;
	public $deleted;
	public $hidden;
	public $administrator;
	public $firstname;
	public $lastname;
	public $email;
	public $username;
	public $password;
	public $createdDate;
	public $updatedDate;

}